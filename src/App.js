import { useState, useEffect } from 'react'
import './App.css'
import Bio from './components/Bio'
import Footer from './components/Footer'
import ToggleButton from './components/ToggleButton'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Posts from './components/Posts'
import { FaMoon } from 'react-icons/fa'
import Content from './components/Content'
import SortBar from './components/SortBar'

function App() {

  const [darkMode, setDarkMode] = useState(false)
  const [data, setData] = useState([])
  const [url, setUrl] = useState("https://blogged-for-you.herokuapp.com/api/all-posts/")

  useEffect(() => {
    const getData = () => {

      fetch(url
        , {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        }
      )
        .then(function (response) {
          return response.json();
        })
        .then(function (myJson) {
          setData(myJson)
        })
    }
    getData()
  }, [darkMode, url])


  const Home = () => {

    return (<>
      <div className="header">{darkMode ? <h1 style={{ color: 'red'}}>B̸͚͎̪̙̀̀́͠l̴̖̟̥͙͎̤͙̔̃͋͠͝ŏ̷̡̱̞̯̘̈̈͐́g̴̨̙͕̪̭̮̪͓̀̈̓̓̎̀̀̀͘͝-̸̢̢̬̖͓̠͈̜̍̔̉̇C̵̗͐̋h̴̤̮͕̰͖͚͂̌̍͜a̷̧̺̪̮̽̓̈̄̄̀̍́̚͝ͅņ̶̛̳̰͙̩̟͍͑͛̉̽̔̆͌̆6̸̤͒̀̾̔6̴̥̈́̈́́̎̄́́̎̓6̷̱͓̰͈̌̈́̿͑̔</h1> : <h1>Blog-Chan</h1>}<label style={{ fontSize: '12px', display: 'flex', justifyContent: 'flex-end', marginTop: '-110px', marginBottom: '100px'}}>{darkMode ? '' : <FaMoon />}<ToggleButton checked={darkMode} onClick={() => setDarkMode(!darkMode)} /></label></div>
    </>
    )
  }

  return (
    <div style={darkMode ? {backgroundColor: '#221f14'} : {backgroundColor: '#e6ffff'}} className="body">

      <Router>

        <Home />

        <div className="container">

          <Route path="/" exact render={() => (<div style={{ display: 'flex', flexDirection: 'column'}}>
            <Bio mode={darkMode} />
            <SortBar url={url} setUrl={setUrl} />
            <Posts data={data} darkMode={darkMode} />
          </div>)}
          />

          {data.map((item) => (
            <div key={item.id}>
              <Route path={`/${item.id}`} exact render={() => <Content item={item} darkMode={darkMode}/>} />
            </div>
          ))}

        </div>

        <Footer mode={darkMode} />

      </Router></div>);
}

export default App;
