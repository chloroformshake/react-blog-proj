import React from 'react'
import './style/Bio.css'
import darkDp from "../images/dp.jpg"
import dp from "../images/dp.jpeg"
import { Link } from "react-router-dom"

const Bio = (props) => {
    return (
        <div className="bio-container">
            <img src={props.mode ? darkDp : dp} alt="dp" />
            <div className="about">
                <div>A sample blog by <Link style={{ color: 'black'}} to={props.mode ? { pathname: "https://instagram.com/chloroformshake" } : { pathname: "https://linkedin.com/in/siddharth-kushwaha" }} target="_blank">Siddharth Kushwaha</Link></div>
                <div>{props.mode ? 'Peace, please' : 'Peace'} </div>
            </div>
        </div>
    )
}

export default Bio
