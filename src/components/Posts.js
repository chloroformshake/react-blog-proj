import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import './style/Posts.css'

function parseDate(input) {
    const newDate = new Date(input)
    return moment(newDate).format('MMMM Do YYYY, h:mm a')
}

const Posts = ({ data, darkMode }) => {

    const [value, setValue] = useState('')

    return (
        <>
            <div style={{display: 'flex', flexDirection: 'column'}}><input id="search-bar" style={ darkMode ? {
                alignSelf: 'flex-start', position: 'relative', top: '-45px', height: '40px'
            } : {
                background: 'transparent', border: 'none', borderBottom: '2px solid black', alignSelf: 'flex-start', position: 'relative', top: '-45px', height: '40px'
            }} type="text" placeholder="Search by Title..." onChange={(event) => setValue(event.target.value)} />
                {data.filter(item => {
                    if (value === "") { return true }
                    else if (item.title.toLowerCase().includes(value.toLowerCase())) { return true }
                    else { return false }
                }).map((item, key) => {
                    key = item.id
                    return (<div id="card" key={key}>
                        <Link id="post-title" to={`/${key}`}>{item.title}</Link>
                        <div id="time">{parseDate(item.createdAt)}</div>
                        <div id="author">&nbsp;-{item.author.name}<span>&nbsp;&nbsp;({item.author.email})</span></div>
                        <br /><br /></div>)
                })}</div>
        </>
    )
}

export default Posts
