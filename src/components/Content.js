import React from 'react'
import ReactMarkdown from 'react-markdown'
import gfm from 'remark-gfm'

const Content = ({ item, darkMode }) => {

    return (<div style={darkMode ? { color: 'red', width: '800px' } : { color: 'black', width: '800px' }}>
        <h2>{item.title}</h2>
        <ReactMarkdown remarkPlugins={[[gfm, { singleTilde: false }]]}>
            {item.content}
        </ReactMarkdown>
    </div>)
}

export default Content
