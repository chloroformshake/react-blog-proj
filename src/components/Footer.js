import React from 'react'
import { Link } from 'react-router-dom'
import './style/Footer.css'

const Footer = (props) => {
    return (
        <div className="footer">
            {props.mode ?
                <Link to={{ pathname: "https://documentingreality.com" }} target="_blank" >Reality</Link> : <><Link to={{ pathname: "https://twitter.com/chloroformshake" }} target="_blank">Twitter</Link> • <Link to={{ pathname: "https://github.com" }} target="_blank">Github</Link> • <Link to={{ pathname: "https://stackoverflow.com" }} target="_blank">Stack Overflow</Link></>}
        </div>
    )
}

export default Footer
