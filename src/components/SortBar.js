import React from 'react'
import './style/SortBar.css'

const SortBar = (props) => {

    const setNewUrl = props.setUrl

    return (
        <>
            <details className="custom-select" >
                <summary className="radios">
                    <input type="radio" value="https://blogged-for-you.herokuapp.com/api/all-posts/" onChange={event => setNewUrl(event.target.value)} name="item" id="default" title="Sort by Oldest" checked />
                    <input type="radio" value="https://blogged-for-you.herokuapp.com/api/all-posts/" onChange={event => setNewUrl(event.target.value)} name="item" id="item1" title="Sort by Oldest" />
                    <input type="radio" value="https://blogged-for-you.herokuapp.com/api/all-posts/?sort=newest" onChange={event => setNewUrl(event.target.value)} name="item" id="item2" title="Sort by Newest" />
                </summary>
                <ul className="list">
                    <li>
                        <label htmlFor="item1">Sort by Oldest</label>
                    </li>
                    <li>
                        <label htmlFor="item2">Sort by Newest</label>
                    </li>
                </ul>
            </details>
        </>
    )
}

export default SortBar
